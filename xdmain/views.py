from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.views.generic import View
from django.contrib.auth.models import User

# Create your views here.


class RegisterView(View):
    template_name = "xdmain/register.html"

    def get(self, request):
        return render(request, self.template_name, context=None)

    def post(self, request):
        data = request.POST
        errors = []
        redirect_to_register = False

        username = str(data["username"])
        email = str(data["email"]).lower()
        password = str(data["password"])
        passwordconfirm = str(data["passwordconfirm"])

        if len(username) < 3:
            errors.append("ユーザー名は3文字以上にしましょう")
        if User.objects.filter(username=username).exists():
            errors.append("このユーザー名はすでに使われています")

        if len(email) < 3 or "@" not in str(email):
            errors.append("不正なメールアドレスです")
        if User.objects.filter(email=email).exists():
            errors.append("このメールアドレスはすでに使われています")

        if password != passwordconfirm:
            errors.append("再入力されたパスワードが違います")

        if errors != []:
            return render(request, self.template_name, context={"errors": errors})

        # from here there are no errors with the data
        user = User()
        user.username = username
        user.email = email
        user.set_password(password)
        user.save()

        if(user.is_active):
            login(request, user)
            return redirect("jihanki:index")

        return render(request, self.template_name, context={"errors": ["Somekind of Eroor"]})


class LoginView(View):
    template_name = "xdmain/login.html"

    def get(self, request):
        return render(request, self.template_name, context=None)

    def post(self, request):
        try:
            username = User.objects.get(
                email=request.POST["email"].lower()).username
            password = request.POST["password"]
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect("jihanki:index")
        except:
            return render(request, self.template_name, context={"errors": ["Error"]})


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("jihanki:index")
