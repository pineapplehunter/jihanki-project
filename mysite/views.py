from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.views.generic import View


class ToJihankiRedirect(View):
    def get(self, request):
        return redirect("jihanki:index")
