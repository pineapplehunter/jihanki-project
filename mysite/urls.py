from django.conf.urls import url,include
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^music/',include('music.urls')),
    url(r'^jihanki/',include('jihanki.urls')),
    url(r'^xdmain/',include('xdmain.urls')),
    url(r'^$',views.ToJihankiRedirect.as_view(),name="mainindex"),
]
